package Datenbank;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Model.Grundst�ck;

public class Grundst�ckDB {
	
	private Datenbank datenbank;
	private static ArrayList <Grundst�ck> grundst�cke = new ArrayList<>();
	
	public Grundst�ckDB(Datenbank datenbank) {
		this.datenbank = datenbank;
	}

	public void add(Grundst�ck grundst�ck) throws SQLException {
		datenbank.einlesen("INSERT INTO monopolygrundst�cke.grundst�cke (NUMMER, NAME, KAUFPREIS, MIETE) VALUES ('" + grundst�ck.getNummer() + "', '"+ grundst�ck.getName() + "', '" + grundst�ck.getKaufpreis() + "','" + grundst�ck.getMiete() + "')");
	}
	
	public Grundst�ck getByNumber(int nummer) throws SQLException {
		ResultSet result = datenbank.auslesen("SELECT * FROM monopolydatenbank.grundst�cke WHERE NUMMER = "+nummer+"");
		while (result.next()) {
			grundst�cke.add(new Grundst�ck(result.getInt("NUMMER"), result.getString("NAME"), result.getInt("KAUFPREIS"), result.getInt("MIETE")));
		}
		return grundst�cke.get(nummer);
	}
	
}