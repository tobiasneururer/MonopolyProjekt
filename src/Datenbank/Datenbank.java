package Datenbank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Datenbank {

	private static Connection verbindung;
	
	private final static String ERSTELLE_TABELLE_ANWEISUNG = 
		"CREATE TABLE IF NOT EXISTS monopolydatenbank.grundstücke (" + 
		"ID INT NOT NULL AUTO_INCREMENT, " + 
		"NUMMER INT, " + 
		"NAME VARCHAR(32), " + 
		"KAUFPREIS INT, " +
		"MIETE INT, " +
		"PRIMARY KEY (ID) " +
		")";
	
	public Datenbank() throws SQLException, ClassNotFoundException {
		verbindung = erstelleVerbindung();
		erstelleTabelle(verbindung);
	}
	
	public Connection erstelleVerbindung() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		verbindung = DriverManager.getConnection("jdbc:mysql://localhost:3306","root","");
		return verbindung; 
	}
	
	public void erstelleTabelle(Connection verbindung) throws ClassNotFoundException, SQLException { 
		Statement anweisung = verbindung.createStatement(); 
		anweisung.executeUpdate(ERSTELLE_TABELLE_ANWEISUNG); 
		anweisung.close(); 
	}
	
	public void einlesen(String query) throws SQLException {
		Statement anweisung = verbindung.createStatement();
		anweisung.executeUpdate(query);
		anweisung.close();
	}
	
	public ResultSet auslesen(String query) throws SQLException { 
		PreparedStatement abfrage = verbindung.prepareStatement(query);
		ResultSet result = abfrage.executeQuery();
		return result;
	}
	
}