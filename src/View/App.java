package View;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class App extends Application {
	@Override
	public void start(Stage stage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("view.fxml"));
        
        stage.setTitle("Monopoly Gurgltal");
        stage.setResizable(false);
        stage.setScene(new Scene(root));
        stage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
}