package Model;

public class Grundstück {

	private int nummer;
	private String name;
	private int kaufpreis;
	private int miete;
	
	public Grundstück(int nummer, String name, int kaufpreis, int miete) {
		this.nummer = nummer;
		this.name = name;
		this.kaufpreis = kaufpreis;
		this.miete = miete;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKaufpreis() {
		return kaufpreis;
	}

	public void setKaufpreis(int kaufpreis) {
		this.kaufpreis = kaufpreis;
	}

	public int getMiete() {
		return miete;
	}

	public void setMiete(int miete) {
		this.miete = miete;
	}
	
	@Override
	public String toString() {
		return this.nummer + " - " + this.name;
	}
	
}