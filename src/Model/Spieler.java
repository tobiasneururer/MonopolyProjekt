package Model;

import java.util.ArrayList;

public class Spieler {

	private String name;
	private int geldbetrag;
	private int position;
	private ArrayList<Grundstück> grundstückeInBesitz;
	
	public Spieler(String name) {
		this.name = name;
		this.geldbetrag = 5000000;
		this.position = 0;
		this.grundstückeInBesitz = new ArrayList<Grundstück>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGeldbetrag() {
		return geldbetrag;
	}

	public void setGeldbetrag(int geldbetrag) {
		this.geldbetrag = geldbetrag;
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}

	public ArrayList<Grundstück> getGrundstückeInBesitz() {
		return grundstückeInBesitz;
	}
	
	public void grundstückHinzufügen(Grundstück grundstück){
		grundstückeInBesitz.add(grundstück);
	}
	
}