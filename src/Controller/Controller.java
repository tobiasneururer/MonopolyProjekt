package Controller;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import Datenbank.Datenbank;
import Datenbank.GrundstückDB;
import Model.Grundstück;
import Model.Spieler;
import Model.Würfel;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Controller {	
	@FXML
	GridPane spielbrett;
	
	@FXML
	Button spielerAnzeigen;
	
	@FXML
	Button bewegen;
	
	@FXML
	Button grundstueckKaufen1;
	
	@FXML
	Button grundstueckKaufen2;
	
	@FXML
	Button start;
	
	@FXML
	Button beenden;
	
	@FXML
	AnchorPane feld0;
	
	@FXML
	AnchorPane feld1;
	
	@FXML
	AnchorPane feld2;
	
	@FXML
	AnchorPane feld3;
	
	@FXML
	AnchorPane feld4;
	
	@FXML
	AnchorPane feld5;
	
	@FXML
	AnchorPane feld6;
	
	@FXML
	AnchorPane feld7;
	
	@FXML
	AnchorPane feld8;
	
	@FXML
	AnchorPane feld9;
	
	@FXML
	AnchorPane feld10;
	
	@FXML
	AnchorPane feld11;
	
	@FXML
	AnchorPane feld12;
	
	@FXML
	TextField text0;
	
	@FXML
	TextField text1;
	
	@FXML
	TextField text2;
	
	@FXML
	TextField text3;
	
	@FXML
	TextField text4;
	
	@FXML
	TextField text5;
	
	@FXML
	TextField text6;
	
	@FXML
	TextField text7;
	
	@FXML
	TextField text8;
	
	@FXML
	TextField text9;
	
	@FXML
	TextField text10;
	
	@FXML
	TextField text11;
	
	@FXML
	TextField text12;
	
	@FXML
	TextField text13;
	
	@FXML
	TextField text14;
	
	@FXML
	TextField text15;
	
	@FXML
	TextField text16;
	
	@FXML
	TextField text17;
	
	@FXML
	TextField text18;
	
	@FXML
	TextField text19;
	
	@FXML
	TextField text20;
	
	@FXML
	TextField text21;
	
	@FXML
	TextField text22;
	
	@FXML
	TextField text23;
	
	@FXML
	TextArea textArea;
	
	Datenbank datenbank; 
	GrundstückDB grundstückdatenbank;
	
	Grundstück grundstück;
	Grundstück grundstück1;
	Grundstück grundstück2;
	Grundstück grundstück3;
	Grundstück grundstück4;
	Grundstück grundstück5;
	Grundstück grundstück6;
	Grundstück grundstück7;
	Grundstück grundstück8;
	Grundstück grundstück9;
	Grundstück grundstück10;
	Grundstück grundstück11;
	
	Spieler spieler1;
	Spieler spieler2;
	Circle circle1 = new Circle(20,Color.GREEN);
	Circle circle2 = new Circle(20,Color.RED);
	
	Würfel würfel;
	
	@FXML
	public void start() throws SQLException, ClassNotFoundException {
		grundstückeErstellen();
		spielerErstellen();
		start.setDisable(true);
	}
	
	@FXML
	public void beenden() {
		System.exit(0);
	}
	
	public void spielerErstellen(){
		spieler1 = new Spieler("Josef");
		spieler2 = new Spieler("Tobias");
		spielbrett.getChildren().add(circle1);
		spielbrett.getChildren().add(circle2);
		spielerZeichnen(circle1, 1, 1);
		spielerZeichnen(circle2, 1, 1);
		textArea.clear();
		textArea.setText("Die Spieler wurden erfolgreich erstellt!");
	}
	
	@FXML
	public void spielerAnzeigen() {
		textArea.clear();
		textArea.setText(spieler1.getName() + ", " + spieler1.getGeldbetrag() + " €, " + spieler1.getGrundstückeInBesitz()+"\n");
		textArea.setText(textArea.getText() + spieler2.getName() + ", " + spieler2.getGeldbetrag() + " €, " + spieler2.getGrundstückeInBesitz()+"\n");
	}
	
	@SuppressWarnings("static-access")
	public void spielerZeichnen(Circle circle, int spalte, int reihe){
		spielbrett.setColumnIndex(circle, spalte);
		spielbrett.setRowIndex(circle, reihe);
		spielbrett.setHalignment(circle, HPos.CENTER);
		spielbrett.setValignment(circle, VPos.CENTER);
	}
	
	public void grundstückeErstellen() throws SQLException, ClassNotFoundException {
		datenbank = new Datenbank();
		grundstückdatenbank = new GrundstückDB(datenbank);
		grundstück = grundstückdatenbank.getByNumber(0);
		grundstück1 = grundstückdatenbank.getByNumber(1);
		grundstück2 = grundstückdatenbank.getByNumber(2);
		grundstück3 = grundstückdatenbank.getByNumber(3);
		grundstück4 = grundstückdatenbank.getByNumber(4);
		grundstück5 = grundstückdatenbank.getByNumber(5);
		grundstück6 = grundstückdatenbank.getByNumber(6);
		grundstück7 = grundstückdatenbank.getByNumber(7);
		grundstück8 = grundstückdatenbank.getByNumber(8);
		grundstück9 = grundstückdatenbank.getByNumber(9);
		grundstück10 = grundstückdatenbank.getByNumber(10);
		grundstück11 = grundstückdatenbank.getByNumber(11);
		
		text0.setText(grundstück.getName());
		text1.setText(grundstück.getKaufpreis() + " €");
		text2.setText(grundstück1.getName());
		text3.setText(grundstück1.getKaufpreis() + " €");
		text4.setText(grundstück2.getName());
		text5.setText(grundstück2.getKaufpreis() + " €");
		text6.setText(grundstück3.getName());
		text7.setText(grundstück3.getKaufpreis() + " €");
		text8.setText(grundstück4.getName());
		text9.setText(grundstück4.getKaufpreis() + " €");
		text10.setText(grundstück5.getName());
		text11.setText(grundstück5.getKaufpreis() + " €");
		text12.setText(grundstück6.getName());
		text13.setText(grundstück6.getKaufpreis() + " €");
		text14.setText(grundstück7.getName());
		text15.setText(grundstück7.getKaufpreis() + " €");
		text16.setText(grundstück8.getName());
		text17.setText(grundstück8.getKaufpreis() + " €");
		text18.setText(grundstück9.getName());
		text19.setText(grundstück9.getKaufpreis() + " €");
		text20.setText(grundstück10.getName());
		text21.setText(grundstück10.getKaufpreis() + " €");
		text22.setText(grundstück11.getName());
		text23.setText(grundstück11.getKaufpreis() + " €");
	}
	
	public void startGeld(Spieler spieler){
		spieler.setGeldbetrag(spieler.getGeldbetrag() + 200000);
		textArea.clear();
		textArea.setText(spieler.getName() + " bekommt 200 000 €, weil er das Startfeld überquert hat!");
	}
	
	public void mieteBezahlen(Grundstück grundstück) throws SQLException {
		int miete = grundstück.getMiete();
		if(spieler1.getGrundstückeInBesitz().contains(grundstück)) {
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - miete);
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() + miete);
			textArea.clear();
			textArea.setText(grundstück.getName() + " gehört bereits " + spieler1.getName() + "!\n");
			textArea.setText(textArea.getText() + spieler2.getName() + " musste " + miete + " € Miete an " + spieler1.getName() + " bezahlen!");
		} else if (spieler2.getGrundstückeInBesitz().contains(grundstück)) {
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - miete);
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() + miete);
			textArea.clear();
			textArea.setText(grundstück.getName() + " gehört bereits " + spieler2.getName() + "!\n");
			textArea.setText(textArea.getText() + spieler1.getName() + " musste " + miete + " € Miete an " + spieler2.getName() + " bezahlen!");
		}
	}
	
	public void keinGeldMehr(Spieler spieler) {
		if(spieler.getGeldbetrag() <= 0) {
			JOptionPane.showMessageDialog(null, "Das Spiel ist beendet!\n" + spieler.getName() + " hat kein Geld mehr!");
			spielerAnzeigen.setDisable(true);
			bewegen.setDisable(true);
			grundstueckKaufen1.setDisable(true);
			grundstueckKaufen2.setDisable(true);
			start.setDisable(true);
		}
	}
	
	@FXML
	public void grundstueckKaufen1() throws SQLException {
		int preis1 = 0;
		switch(spieler1.getPosition()) {
		case 0:
			textArea.clear();
			textArea.setText("Dieses Grundstück kann nicht gekauft werden!");
			break;
		case 1:
			keinGeldMehr(spieler1);
			preis1 = grundstück.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück.getName() + " erworben!");
			break;
		case 2:
			keinGeldMehr(spieler1);
			preis1 = grundstück1.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück1);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück1.getName() + " erworben!");
			break;
		case 3:
			keinGeldMehr(spieler1);
			preis1 = grundstück2.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück2);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück2.getName() + " erworben!");
			break;
		case 4:
			keinGeldMehr(spieler1);
			preis1 = grundstück3.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück3);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück3.getName() + " erworben!");
			break;
		case 5:
			keinGeldMehr(spieler1);
			preis1 = grundstück4.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück4);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück4.getName() + " erworben!");
			break;
		case 6:
			keinGeldMehr(spieler1);
			preis1 = grundstück5.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück5);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück5.getName() + " erworben!");
			break;
		case 7:
			keinGeldMehr(spieler1);
			preis1 = grundstück6.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück6);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück6.getName() + " erworben!");
			break;
		case 8:
			keinGeldMehr(spieler1);
			preis1 = grundstück7.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück7);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück7.getName() + " erworben!");
			break;
		case 9:
			keinGeldMehr(spieler1);
			preis1 = grundstück8.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück8);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück8.getName() + " erworben!");
			break;
		case 10:
			keinGeldMehr(spieler1);
			preis1 = grundstück9.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück9);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück9.getName() + " erworben!");
			break;
		case 11:
			keinGeldMehr(spieler1);
			preis1 = grundstück10.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück10);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück10.getName() + " erworben!");
			break;
		case 12:
			keinGeldMehr(spieler1);
			preis1 = grundstück11.getKaufpreis();
			spieler1.setGeldbetrag(spieler1.getGeldbetrag() - preis1);
			spieler1.grundstückHinzufügen(grundstück11);
			textArea.clear();
			textArea.setText(spieler1.getName() + " hat " + grundstück11.getName() + " erworben!");
			break;
		default:
			spieler1.setPosition(0);
			break;
		}
	}
	
	@FXML
	public void grundstueckKaufen2() throws SQLException {
		int preis2 = 0;
		switch(spieler2.getPosition()) {
		case 0:
			textArea.clear();
			textArea.setText("Dieses Grundstück kann nicht gekauft werden!");
			break;
		case 1:
			keinGeldMehr(spieler2);
			preis2 = grundstück.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück.getName() + " erworben!");
			break;
		case 2:
			keinGeldMehr(spieler2);
			preis2 = grundstück1.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück1);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück1.getName() + " erworben!");
			break;
		case 3:
			keinGeldMehr(spieler2);
			preis2 = grundstück2.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück2);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück2.getName() + " erworben!");
			break;
		case 4:
			keinGeldMehr(spieler2);
			preis2 = grundstück3.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück3);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück3.getName() + " erworben!");
			break;
		case 5:
			keinGeldMehr(spieler2);
			preis2 = grundstück4.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück4);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück4.getName() + " erworben!");
			break;
		case 6:
			keinGeldMehr(spieler2);
			preis2 = grundstück5.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück5);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück5.getName() + " erworben!");
			break;
		case 7:
			keinGeldMehr(spieler2);
			preis2 = grundstück6.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück6);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück6.getName() + " erworben!");
			break;
		case 8:
			keinGeldMehr(spieler2);
			preis2 = grundstück7.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück7);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück7.getName() + " erworben!");
			break;
		case 9:
			keinGeldMehr(spieler2);
			preis2 = grundstück8.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück8);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück8.getName() + " erworben!");
			break;
		case 10:
			keinGeldMehr(spieler2);
			preis2 = grundstück9.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück9);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück9.getName() + " erworben!");
			break;
		case 11:
			keinGeldMehr(spieler2);
			preis2 = grundstück10.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück10);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück10.getName() + " erworben!");
			break;
		case 12:
			keinGeldMehr(spieler2);
			preis2 = grundstück11.getKaufpreis();
			spieler2.setGeldbetrag(spieler2.getGeldbetrag() - preis2);
			spieler2.grundstückHinzufügen(grundstück11);
			textArea.clear();
			textArea.setText(spieler2.getName() + " hat " + grundstück11.getName() + " erworben!");
			break;
		default:
			spieler2.setPosition(0);
			break;
		}
	}

	@FXML
	public void bewegen() throws SQLException {
		würfel = new Würfel();
		int gewuerfelterWert1 = würfel.würfeln();
		int gewuerfelterWert2 = würfel.würfeln();
		
		textArea.clear();
		textArea.setText(spieler1.getName() + " hat " + gewuerfelterWert1 + " gewürfelt!\n");
		textArea.setText(textArea.getText() + spieler2.getName() + " hat " + gewuerfelterWert2 + " gewürfelt!");
		
		spieler1.setPosition(spieler1.getPosition()+gewuerfelterWert1);
		spieler2.setPosition(spieler2.getPosition()+gewuerfelterWert2);
		
		switch(spieler1.getPosition()) {
		case 0:
			spielerZeichnen(circle1, 1, 1);
			startGeld(spieler1);
			break;
		case 1:
			spielerZeichnen(circle1, 2, 1);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück);
			break;
		case 2:
			spielerZeichnen(circle1, 3, 1);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück1);
			break;
		case 3:
			spielerZeichnen(circle1, 4, 1);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück2);
			break;
		case 4:
			spielerZeichnen(circle1, 5, 2);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück3);
			break;
		case 5:
			spielerZeichnen(circle1, 5, 3);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück4);
			break;
		case 6:
			spielerZeichnen(circle1, 5, 4);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück5);
			break;
		case 7:
			spielerZeichnen(circle1, 4, 5);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück6);
			break;
		case 8:
			spielerZeichnen(circle1, 3, 5);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück7);
			break;
		case 9:
			spielerZeichnen(circle1, 2, 5);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück8);
			break;
		case 10:
			spielerZeichnen(circle1, 1, 4);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück9);
			break;
		case 11:
			spielerZeichnen(circle1, 1, 3);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück10);
			break;
		case 12:
			spielerZeichnen(circle1, 1, 2);
			keinGeldMehr(spieler1);
			mieteBezahlen(grundstück11);
			break;
		default:
			spieler1.setPosition(0);
			spielerZeichnen(circle1, 1, 1);
			startGeld(spieler1);
			break;
		}
		
		switch(spieler2.getPosition()) {
		case 0:
			spielerZeichnen(circle2, 1, 1);
			startGeld(spieler2);
			break;
		case 1:
			spielerZeichnen(circle2, 2, 1);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück);
			break;
		case 2:
			spielerZeichnen(circle2, 3, 1);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück1);
			break;
		case 3:
			spielerZeichnen(circle2, 4, 1);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück2);
			break;
		case 4:
			spielerZeichnen(circle2, 5, 2);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück3);
			break;
		case 5:
			spielerZeichnen(circle2, 5, 3);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück4);
			break;
		case 6:
			spielerZeichnen(circle2, 5, 4);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück5);
			break;
		case 7:
			spielerZeichnen(circle2, 4, 5);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück6);
			break;
		case 8:
			spielerZeichnen(circle2, 3, 5);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück7);
			break;
		case 9:
			spielerZeichnen(circle2, 2, 5);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück8);
			break;
		case 10:
			spielerZeichnen(circle2, 1, 4);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück9);
			break;
		case 11:
			spielerZeichnen(circle2, 1, 3);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück10);
			break;
		case 12:
			spielerZeichnen(circle2, 1, 2);
			keinGeldMehr(spieler2);
			mieteBezahlen(grundstück11);
			break;
		default:
			spieler2.setPosition(0);
			spielerZeichnen(circle2, 1, 1);
			startGeld(spieler2);
			break;
		}
	}
	
}